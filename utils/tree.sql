/*SELECT
  sum(app_entry.value_egov)
FROM
  app_entry
      JOIN app_organizations on app_entry.organization_id = app_organizations.id
where
  app_organizations.parent_id IN
  (*/WITH
rec (id, id_Owner)
AS (
   SELECT m.id id, m.id_Owner id_Owner FROM dbo.Menu m
    WHERE id_Owner = 9
   UNION ALL
   SELECT Menu.id id, Menu.id_Owner id_Owner
    FROM dbo.Menu JOIN rec ON rec.id = Menu.id_Owner
   )
SELECT id, id_Owner FROM rec
 GO


/*    (SELECT
      org.id
      FROM
        app_organizations org
      WHERE
           org.id = 9 )*/