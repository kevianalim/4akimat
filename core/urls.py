"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers
from app import views as app
from rest_framework_jwt.views import obtain_jwt_token

router = routers.DefaultRouter()
router.register(r'reports', app.ReportsViewSet)
router.register(r'organizations', app.OrganizationViewSet)


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/indicators/(?P<indicator>[0-9]+)/$', app.get_indicator),
    url(r'^api/indicators/add-executors/$', app.add_executors),
    url(r'^api/indicators/delete-executor/$', app.delete_executor),
    url(r'^api/reports/create-indicator/$', app.create_indicator),
    url(r'^api/reports/edit-indicator/$', app.edit_indicator),
    url(r'^api/reports/(?P<report>[0-9]+)/report-details/$', app.report_details),
    url(r'^api/reports/(?P<report>[0-9]+)/indicators-list/$', app.get_indicators_list),
    url(r'^api/reports/fill/$', app.fill_report),
    url(r'^api/organizations/create-organization/$', app.create_organization),
    url(r'^api/organizations/(?P<org>[0-9]+)/$', app.organization_details),
    url(r'^api/organizations/get-childrens/$', app.get_childrens_for_indicator),
    url(r'^api/organizations/delete-children/$', app.delete_children),
    url(r'^api/organizations/delete-organization/(?P<organization>[0-9]+)/$', app.delete_organization),
    url(r'^api/users/create-user/$', app.create_user),
    url(r'^api/users/users-list/$', app.users_list),
    url(r'^api/users/delete/(?P<user>[0-9]+)/$', app.delete_user),
    url(r'^api/entries/create-entry/$', app.create_entry),
    url(r'^api/entries/fetch-entries/$', app.fetch_entries),
    url(r'^api/', include(router.urls)),
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^$', app.index)
]
