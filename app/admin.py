from django.contrib import admin
from mptt.admin import DraggableMPTTAdmin
from app.models import Organization, Report, Indicator, Entry, User, IndicatorsOrgsRelation



# Register your models here.
admin.site.register(
    Organization,
    DraggableMPTTAdmin,
    list_display=(
        'tree_actions',
        'indented_title'
    ),
    list_display_links=(
        'indented_title',
    ))
admin.site.register(Report)
admin.site.register(Indicator)
admin.site.register(Entry)
admin.site.register(User)
admin.site.register(IndicatorsOrgsRelation)
