from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.utils.translation import ugettext_lazy as _
from app.managers import UserManager


# Create your models here.
class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email address'), unique=True)
    first_name = models.CharField(_('first name'), max_length=50, blank=True)
    sur_name = models.CharField(_('first name'), max_length=50, blank=True)
    last_name = models.CharField(_('last name'), max_length=50, blank=True)
    phone = models.CharField(_('phone number'), max_length=20, blank=True)
    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
    is_active = models.BooleanField(_('active'), default=True)
    is_staff = models.BooleanField(_('active'), default=False)
    organization = models.ForeignKey('Organization', blank=True, null=True, related_name='users')

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        ordering = ['-id']
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_full_name(self):
        full_name = '%s %s %s' % (self.sur_name, self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        return self.first_name


class Report(models.Model):
    name = models.CharField(max_length=255)
    day = models.PositiveIntegerField(default=1)
    month = models.PositiveIntegerField(default=3)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Форма'
        verbose_name_plural = 'Формы'


class Indicator(models.Model):
    report = models.ForeignKey(Report, on_delete=models.DO_NOTHING, blank=True, null=True, related_name='indicators')
    name = models.CharField(max_length=255, blank=False, null=False)
    code = models.CharField(max_length=255)

    def __str__(self):
        return self.code + ' - ' + self.name

    class Meta:
        verbose_name = 'Показатель'
        verbose_name_plural = 'Показатели'


class Organization(MPTTModel):
    name = models.CharField(max_length=255)
    parent = TreeForeignKey('self', blank=True, null=True, related_name='childrens', db_index=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Организация'
        verbose_name_plural = 'Организации'


class IndicatorsOrgsRelation(models.Model):
    indicator = models.ForeignKey(Indicator, on_delete=models.DO_NOTHING, related_name='organizations_rel')
    organization = models.ForeignKey(Organization, on_delete=models.DO_NOTHING, related_name='indicators_rel')
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.indicator.name + ' ' + self.organization.name

    class Meta:
        verbose_name = 'Связь организаций с показателями'
        verbose_name_plural = 'Связи организаций с показателями'


class Entry(models.Model):
    value_gov_org = models.PositiveIntegerField(default=0)
    value_gov_comp = models.PositiveIntegerField(default=0)
    value_egov = models.PositiveIntegerField(default=0)
    indicator_org = models.ForeignKey(IndicatorsOrgsRelation, on_delete=models.DO_NOTHING, related_name='entrys')
    indicator = models.ForeignKey(Indicator, on_delete=models.DO_NOTHING, related_name='entrys')
    organization = models.ForeignKey(Organization, on_delete=models.DO_NOTHING, related_name='entrys')
    date_fill = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.indicator.name + ' - ' + self.organization.name + ' ({})'.format(str(self.date_fill))

    class Meta:
        verbose_name = 'Запись'
        verbose_name_plural = 'Записи'
