from app.models import Report, Indicator, IndicatorsOrgsRelation, Organization, Entry, User
from rest_framework import serializers


class ReportsSerializer(serializers.ModelSerializer):
    indicators_count = serializers.IntegerField(source='indicators.count', read_only=True)
    label = serializers.SerializerMethodField()

    def get_label(self, obj):
        return obj.name

    class Meta:
        model = Report
        fields = ('id', 'name', 'day', 'month', 'indicators_count', 'label')


class IndicatorSerializer(serializers.ModelSerializer):
    label = serializers.SerializerMethodField()

    def get_label(self, obj):
        return obj.name

    class Meta:
        model = Indicator
        fields = ('id', 'name', 'report', 'code', 'label')


class IndicatorSerializerForDetails(serializers.ModelSerializer):
    executors_count = serializers.SerializerMethodField()

    def get_executors_count(self, obj):
        return obj.executors_count

    class Meta:
        model = Indicator
        fields = ('id', 'name', 'code', 'executors_count')


class IndicatorsOrgsRelationSerializer(serializers.ModelSerializer):
    class Meta:
        model = IndicatorsOrgsRelation
        fields = ('id', 'indicator', 'organization', 'active')


class OrganizationSerializer(serializers.ModelSerializer):
    users_count = serializers.IntegerField(source='users.count', read_only=True)
    label = serializers.SerializerMethodField()

    def get_label(self, obj):
        return obj.name

    class Meta:
        model = Organization
        fields = ('id', 'name', 'parent', 'users_count', 'label')


class EntrySerializer(serializers.ModelSerializer):
    indicator_name = serializers.SerializerMethodField()

    def get_indicator_name(self, obj):
        return obj.indicator.name

    class Meta:
        model = Entry
        fields = ('id', 'value_gov_org', 'value_gov_comp', 'value_egov', 'date_fill', 'indicator', 'indicator_name', 'organization')


class UserSerializer(serializers.ModelSerializer):
    organization = OrganizationSerializer(read_only=True)

    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'sur_name', 'last_name', 'organization', 'phone')
