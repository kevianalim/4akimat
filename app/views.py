from app.models import Report, Indicator, IndicatorsOrgsRelation, Organization, Entry, User
from rest_framework import viewsets, views, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from django.shortcuts import render, redirect
import json
import datetime
from app.serializers import ReportsSerializer, IndicatorSerializer, IndicatorsOrgsRelationSerializer,\
    OrganizationSerializer, EntrySerializer, UserSerializer, IndicatorSerializerForDetails


# forms list
class ReportsViewSet(viewsets.ModelViewSet):
    queryset = Report.objects.all()
    serializer_class = ReportsSerializer


# organizations list
class OrganizationViewSet(viewsets.ModelViewSet):
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer


@api_view(['GET'])
def index(request):
    return render(request, 'index.html')


# report details (indicators list)
@api_view(['GET', ])
def report_details(request, report):
    if request.method == 'GET':
        user = User.objects.get(id=request.user.id)
        indicators = Indicator.objects.filter(
            organizations_rel__organization_id=user.organization,
            organizations_rel__active=True,
            report_id=report
        )
        childs = Organization.objects.get(id=user.organization_id).get_children()
        for indicator in indicators:
            indicator.executors_count = IndicatorsOrgsRelation.objects.filter(indicator_id=indicator.id, organization__in=childs, active=True).count()
        report_name = Report.objects.get(id=report).name
        serializer = IndicatorSerializerForDetails(indicators, many=True)
        return Response({'name': report_name, 'indicators': serializer.data})


#
@api_view(['GET'])
def get_indicators_list(request, report):
    maintainer = User.objects.get(id=request.user.id)
    indicators = Indicator.objects.filter(
        organizations_rel__organization_id=maintainer.organization_id,
        organizations_rel__active=True,
        report_id=report)
    print(indicators)
    return Response(IndicatorSerializer(indicators, many=True).data)


#
@api_view(['POST', ])
def create_indicator(request):
    if request.method == 'POST':
        user = User.objects.get(id=request.user.id)
        indicator = Indicator.objects.create(
            name=request.POST['name'],
            code=request.POST['code'],
            report_id=request.POST['report']
        )
        indicator.save()
        relation = IndicatorsOrgsRelation.objects.create(
            indicator=indicator,
            organization_id=user.organization_id,
            active=True
        )
        relation.save()
        return Response({"name": indicator.name, 'code': indicator.code, 'id': indicator.id, 'report': indicator.report.id})


#
@api_view(['POST'])
def edit_indicator(request):
    if request.method == 'POST':
        indicator = Indicator.objects.get(id=request.POST['id'])
        indicator.name = request.POST['name']
        indicator.code = request.POST['code']
        indicator.save()
        return Response({'id': indicator.id, 'name': indicator.name, 'code': indicator.code})


#
@api_view(['GET'])
def organization_details(request, org):
    if request.method == 'GET':
        user = User.objects.get(id=request.user.id)
        organization = Organization.objects.get(id=org)
        childrens = organization.get_children()
        if organization.parent:
            parent = Organization.objects.get(id=organization.parent_id)
            parent = OrganizationSerializer(parent).data
        else:
            parent = None
        childrens_serialized = OrganizationSerializer(childrens, many=True)
        return Response({
            'name': organization.name,
            'id': organization.id,
            'childrens': childrens_serialized.data,
            'parent': parent
        })


#
@api_view(['POST', ])
def create_organization(request):
    if request.method == 'POST':
        maintainer = User.objects.get(id=request.user.id)
        post = request.POST
        if post['parent'] != 'undefined' and post['parent'] != '':
            organization = Organization.objects.create(
                name=post['name'],
                parent_id=post['parent']
            )
        else:
            organization = Organization.objects.create(
                name=post['name'],
            )
        organization.save()
        return Response({'name': organization.name, 'id': organization.id})


#
@api_view(['DELETE'])
def delete_organization(request, organization):
    maintainer = User.objects.get(id=request.user.id)
    org = Organization.objects.get(id=organization)
    childs = org.get_children()
    childs.update(parent=None)
    org.delete()
    return Response('OK')


#
@api_view(['GET'])
def get_indicator(request, indicator):
    if request.method == 'GET':
        user = User.objects.get(id=request.user.id)
        indicator = Indicator.objects.get(id=indicator)
        org = Organization.objects.get(id=user.organization_id)
        orgs = org.get_children()
        relations = IndicatorsOrgsRelation.objects.filter(organization__in=orgs, indicator=indicator, active=True)
        orgs = Organization.objects.filter(indicators_rel__in=relations)
        orgs_data = OrganizationSerializer(orgs, many=True).data
        return Response(
            {
                "name": indicator.name,
                "id": indicator.id,
                "orgs": orgs_data
             }
        )


#
@api_view(['POST', 'GET'])
def get_childrens_for_indicator(request):
    maintainer = User.objects.get(id=request.user.id)
    if request.method == 'POST':
        indicator = Indicator.objects.get(id=request.POST['indicator'])
        org = Organization.objects.get(id=maintainer.organization_id)
        childrens = org.get_children()
        relations = IndicatorsOrgsRelation.objects.filter(indicator_id=indicator.id, organization__in=childrens)
        not_related_childs = Organization.objects.exclude(indicators_rel__in=relations).exclude(id=maintainer.organization_id).filter(id__in=childrens)
        not_related_childs_data = OrganizationSerializer(not_related_childs, many=True).data
        return Response(not_related_childs_data)
    elif request.method == 'GET':
        org = Organization.objects.get(id=maintainer.organization_id)
        childrens = org.get_children()
        all_childrens = OrganizationSerializer(childrens, many=True)
        return Response(all_childrens.data)


#
@api_view(['POST'])
def delete_children(request):
    maintainer = User.objects.get(id=request.user.id)
    post = request.POST
    org = Organization.objects.get(id=post['children'])
    org.parent = None
    org.save()
    return Response('OK')


#
@api_view(['POST'])
def add_executors(request):
    if request.method == 'POST':
        post = json.loads(request.body)
        indicator = Indicator.objects.get(id=post["indicator"])
        organizations = Organization.objects.filter(id__in=post["organizations"])
        for organization in organizations:
            try:
                rel = IndicatorsOrgsRelation.objects.get(indicator=indicator, organization_id=organization.id)
                rel.active = True
                rel.save()
            except:
                rel = IndicatorsOrgsRelation.objects.create(indicator=indicator, organization_id=organization.id, active=True)
        orgs_data = OrganizationSerializer(organizations, many=True).data
        return Response(orgs_data)


#
@api_view(['POST'])
def delete_executor(request):
    if request.method == 'POST':
        post = request.POST
        maintainer = User.objects.get(id=request.user.id)
        org = Organization.objects.get(id=post['organization'])
        print(org)
        orgs = org.get_descendants(include_self=True)
        print(orgs)
        rels = IndicatorsOrgsRelation.objects.filter(organization__in=orgs)
        rels.update(active=False)
        return Response('OK')
    else:
        return Response(status.HTTP_405_METHOD_NOT_ALLOWED, 'Запрещено')


#
@api_view(['POST'])
def create_user(request):
    print(request.POST['email'])
    if request.method == 'POST':
        maintainer = User.objects.get(id=request.user.id)
        if maintainer.has_perm('create', User):
            new_user = User.objects.create_user(
                email=request.POST['email'],
                password=request.POST['password'],
                first_name=request.POST['first_name'],
                sur_name=request.POST['sur_name'],
                last_name=request.POST['last_name'],
                phone=request.POST['phone'],
                organization_id=request.POST['organization']
            )
        else:
            raise RuntimeError
        return Response({'email': new_user.email, 'id': new_user.id})


#
@api_view(['GET'])
def users_list(request):
    if request.method == 'GET':
        maintainer = User.objects.get(id=request.user.id)
        paginator = PageNumberPagination()
        paginator.page_size = 20
        users = User.objects.filter(is_active=True).exclude(id=1)
        result = paginator.paginate_queryset(users, request)
        users_data = UserSerializer(result, many=True).data
        return paginator.get_paginated_response(users_data)


#
@api_view(["DELETE"])
def delete_user(request, user):
    if request.method == 'DELETE':
        maintainer = User.objects.get(id=request.user.id)
        if maintainer.has_perm('delete', User):
            user = User.objects.get(id=user)
            if not maintainer.id == user.id:
                user.is_active = False
                user.is_staff = False
                user.save()
                return Response('OK')
            else:
                return Response(status=status.HTTP_409_CONFLICT, data='Вы пытаетесь удалить себя')


#
@api_view(['POST'])
def create_entry(request):
    if request.method == 'POST':
        maintainer = User.objects.get(id=request.user.id)
        relation = IndicatorsOrgsRelation.objects.get(indicator_id=request.POST['indicator'], organization_id=maintainer.organization_id)
        entry = Entry.objects.create(
            value_gov_org=request.POST['value_org'],
            value_gov_comp=request.POST['value_comp'],
            value_egov=request.POST['value_egov'],
            indicator_org_id=relation.id,
            indicator_id=request.POST['indicator'],
            organization_id=maintainer.organization_id,
        )
        return Response("OK")


#
@api_view(['POST'])
def fetch_entries(request):
    if request.method == 'POST':
        post = json.loads(request.body)
        maintainer = User.objects.get(id=request.user.id)
        descendants = Organization.objects.get(id=maintainer.organization_id).get_descendants(include_self=True)
        beg = datetime.datetime.strptime(post['dateBeg'], '%Y-%m-%dT%H:%M:%S.%fZ')
        end = datetime.datetime.strptime(post['dateEnd'], '%Y-%m-%dT%H:%M:%S.%fZ')
        if post['indicators']:
            entries = Entry.objects.filter(indicator__in=post['indicators'], date_fill__range=(beg, end), organization__in=descendants)
        else:
            entries = Entry.objects.filter(date_fill__range=(beg, end), organization__in=descendants)
        return Response(EntrySerializer(entries, many=True).data)
        # maintainer = User.objects.get(id=request.user.id)
        # orgs = Organization.objects.get(id=maintainer.organization_id).get_descendants(include_self=False)
        # entrys = Entry.objects.filter(organization__in=orgs)


#
@api_view(['POST'])
def check_report_is_filled(request):
    post = request.POST
    maintainer = User.objects.get(id=request.user.id)
    try:
        entry = Entry.objects.filter(date_fill__month=post['month'], date_fill__year=post['year'], organization_id=maintainer.organization_id)
        return Response({'status': 'yes'})
    except:
        return Response({'status': 'no'})


#
@api_view(['POST'])
def fill_report(request):
    post = json.loads(request.body)
    maintainer = User.objects.get(id=request.user.id)
    month = post['month']
    year = post['year']
    for indicator in post['values']:
        relation = IndicatorsOrgsRelation.objects.get(organization_id=maintainer.organization_id, indicator_id=indicator['id'])
        try:
            entry = Entry.objects.get(
                date_fill__month=month,
                date_fill__year=year,
                indicator_id=indicator['id'],
                organization_id=maintainer.organization_id,
                indicator_org_id=relation.id
            )
            entry.value_egov = indicator['egov']
            entry.value_gov_comp = indicator['comp']
            entry.value_gov_org = indicator['org']
            entry.save()
            return Response({'status': 'Отчет обновлен'})
        except Exception as e:
            print(e)
            entry = Entry.objects.create(
                date_fill=datetime.datetime(year, month, 2, 18, 0, 0, 0),
                indicator_id=indicator['id'],
                organization_id=maintainer.organization_id,
                indicator_org_id=relation.id,
                value_egov=indicator['egov'],
                value_gov_comp=indicator['comp'],
                value_gov_org=indicator['org']
            )
            return Response({'status': 'Отчет заполнен'})
