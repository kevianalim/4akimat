import os
import django
import schedule
import time
import datetime
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")
django.setup()
from app.models import Organization, User, Entry
from app.serializers import UserSerializer, EntrySerializer


# Create your tests here.
def djan():
    orgs = Organization.objects.get(id=1).get_descendants(include_self=False)
    entries = Entry.objects.filter(organization__in=orgs).count()
    data = EntrySerializer(entries, many=True)
    print(data.data)


def sched():
    print("I'm working...")

schedule.every()


if __name__ == '__main__':
    djan()
    while True:
        schedule.run_pending()
        time.sleep(1)
